# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import logging


from odoo import api, fields, models, _
from datetime import date,datetime
from dateutil.relativedelta import relativedelta 

_logger = logging.getLogger(__name__)

class TamirciClient(models.Model):
    _name = 'tamirci.client'
    _description = 'Client'
    _rec_name = 'client_id'

    @api.onchange('client_id')
    def _onchange_patient(self):
        '''
        The purpose of the method is to define a domain for the available
        purchase orders.
        '''
        # address_id = self.client_id
        # self.partner_address_id = address_id

    # @api.depends('date_of_birth')
    # def _onchange_age(self):
    #     for rec in self:
    #         if rec.date_of_birth:
    #             d1 = rec.date_of_birth
    #             d2 = datetime.today().date()
    #             rd = relativedelta(d2, d1)
    #             rec.age = str(rd.years) + "y" +" "+ str(rd.months) + "m" +" "+ str(rd.days) + "d"
    #         else:
    #             rec.age = "No Date Of Birth!!"
    name = fields.Char(string='Client ID', readonly=True)
    client_id = fields.Many2one('res.partner', string='Nom du Prestataire', default=lambda self: self.env.user, required=True, copy=False)
    # sex = fields.Selection([('m', 'Male'),('f', 'Female')], string ="Sex")
    # age = fields.Char(compute=_onchange_age,string="Patient Age",store=True)
    # marital_status = fields.Selection([('s','Single'),('m','Married'),('w','Widowed'),('d','Divorced'),('x','Seperated')],string='Marital Status')
    # date_of_birth = fields.Date(string="Date of Birth")
    # last_name = fields.Char('Last Name')
    photo = fields.Binary(string="Picture")
    # education = fields.Selection([('o','None'),('1','Incomplete Primary School'),
    #                               ('2','Primary School'),
    #                               ('3','Incomplete Secondary School'),
    #                               ('4','Secondary School'),
    #                               ('5','University')],string='Education Level')
    # works = fields.Boolean('Works')
    # notes = fields.Text(string="Extra info")
    telephone = fields.Char('Telephone')

    @api.model
    def create(self, vals):
        vals['name'] = self.env['ir.sequence'].next_by_code(
            'tamirci.client') or 'New'
        msg_body = 'Client créé'
        for msg in self:
                msg.message_post(body=msg_body)
        return super(TamirciClient, self).create(vals)

    
