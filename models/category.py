from odoo import _, api, fields, models

class CategoryService(models.Model):
  _name = 'tamirci.category'
  _description = 'name'
  
  name = fields.Char(string="Nom de la catégorie", required=True)
  description = fields.Text(string="Description")
  image = fields.Image("Image", max_width=64, max_height=64)
  service_ids = fields.One2many(
      'service.name', 'category_id', string='Service')