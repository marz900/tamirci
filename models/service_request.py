# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo.exceptions import Warning, ValidationError, UserError
from odoo import api, fields, models, _
from datetime import datetime
import logging


class TamirciRequest(models.Model):
    _name = 'tamirci.request'
    _description = 'Demande de service'
    _rec_name = 'partner_id'

    name = fields.Char(string="Date de demande",
                       default=fields.Datetime.now(), readonly=True, copy=True)
    unique_code = fields.Char(string="Demande De Service ID", readonly=True, copy=True)
    date_prestation = fields.Datetime('Date de demande')
    client_id = fields.Many2one(
        'tamirci.client', string='Client', required=True)
    partner_id = fields.Many2one(
        'res.partner', string='Prestataire', required=True)
    category = fields.Many2one('tamirci.category', string='Categorie')
    service = fields.Many2one('service.name', string='Service')
    localisation = fields.Char('Localisation')
    date_created = fields.Datetime(readonly=True)
    description_pb = fields.Text('Description')
    state = fields.Selection([
        ('draft', 'Draft'),
      ('confirm', 'Confirmed'),
      ('cancel', 'Canceled')
    ], string='Status', readonly=True, default="draft")
    price = fields.Float('Price')
    priority = fields.Selection([
        ('0', 'Low'),
      ('1', 'Medium'),
      ('2', 'High'),
      ('3', 'Very High'),
        ], string='Etoile', default='0')
    image = fields.Image("Image", max_width=64, max_height=64)

    @api.model
    def create(self, vals):
        vals['unique_code'] = self.env['ir.sequence'].next_by_code(
            'tamirci.request') or 'New'
        msg_body = 'Damande de service créée'
        for msg in self:
                msg.message_post(body=msg_body)
        return super(TamirciRequest, self).create(vals)

    def write(self, vals):
        vals['date_created'] = fields.Datetime.now()
        return super(TamirciRequest, self).write(vals)

    def unlink(self):
        for v in self:
            if v.state == 'confirm':
                raise UserError(
                    'Vous ne pouvez pas supprimer les offres confirmées')
        return super(TamirciRequest, self).unlink()

    def confirm(self):
        self.write({'state': 'confirmed'})

    def done(self):
        self.write({'state': 'done'})

    def cancel(self):
        self.write({'state': 'cancel'})
