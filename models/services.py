# -*- coding: utf-8 -*-
from odoo import fields, models, _


class Services(models.Model):
    _name = 'service.name'
    _description = 'Nom du Service'

    name = fields.Char(string='Nom du service', required=True)
    category_id = fields.Many2one('tamirci.category', string='Catégorie')
   