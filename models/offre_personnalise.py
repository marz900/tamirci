# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo.exceptions import Warning, ValidationError, UserError
from odoo import api, fields, models, _
from datetime import datetime
import logging

class OffrePersonnalise(models.Model):
  _name = 'tamirci.offre'
  _description = 'Offre Personnalise'
  _rec = 'unique_code'

  name = fields.Char(default=fields.Datetime.now())
  date_prestation = fields.Datetime('Date de demande', required=True)
  unique_code = fields.Char('ID', readonly=True)
  partner_id = fields.Many2one('res.partner', string='Choisir Prestataire', required=True)
  category = fields.Many2one('tamirci.category', string='Categorie', required=True)
  service = fields.Many2one('service.name', string='Service', required=True)
  date_created = fields.Datetime(readonly=True) 
  description_pb = fields.Text('Description', required=True)
  state = fields.Selection([
    ('draft', 'Draft'),
    ('confirm', 'Confirmed'),
    ('cancel', 'Canceled')
  ], string='Status', readonly=True, default="draft")

  @api.model_create_multi
  def create(self, vals):
    vals['unique_code'] = self.env['ir.sequence'].next_by_code('res.partner.unique.code') or 'New'
    return super(OffrePersonnalise, self).create(vals)

  def write(self, vals):

    vals['date_created'] = fields.Datetime.now() 
    return super(OffrePersonnalise, self).write(vals)
  
  def unlink(self):
    for v in self:
      if v.state == 'confirm':
        raise UserError('Vous ne pouvez pas supprimer les offres confirmées')
    return super(OffrePersonnalise, self).unlink()
  
