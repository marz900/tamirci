# -*- coding: utf-8 -*-

from . import prestataire
from . import services
from . import offre_personnalise
from . import res_partner_inherit
from . import category
from . import client
from . import demande
from . import service_request
