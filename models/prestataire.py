# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import logging


from odoo import fields, models, _

_logger = logging.getLogger(__name__)

class TamirciPrestataire(models.Model):
    _name = 'tamirci.prestataire'
    _description = 'Prestataire'
    _rec_name = 'partner_id'

    partner_id = fields.Many2one('res.partner', string='Nom du Prestataire', default=lambda self: self.env.user, required=True, copy=False)
    code = fields.Char('Id')
    info = fields.Text('Extra Info')
    # name_service = fields.Many2one('service.name', string='Nom du service', required=True)
    # price = fields.Float(string='Prix Unitaire', required=True)
    # type_service = fields.Many2many('types.tag', 'type_service_name', 'service_id', 'type_tag_id', string='Type de service', required=True)
    # description = fields.Text(string='Description du service', required=True)