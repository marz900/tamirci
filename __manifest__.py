# -*- coding: utf-8 -*-
{
    'name': "Tamirci",
    'summary': """
       service sur site""",
    'description': """
        en cours de développement de la première version
    """,
    'author': "Marzouk IDRISSOU",
    'website': "http://www.yourcompany.com",
    'category': 'Productivity',
    'sequence': '-100',
    'version': '1.0',
    'depends': ['base'],
    'data': [
        'security/groupe.xml',
        'data/data.xml',
        'views/main_menu.xml',
        'views/prestataire.xml',
        'views/client.xml',
        'views/category.xml',
        'views/services_views.xml',
        'views/res_partner_inherit.xml',
        'views/offre_personnalise.xml',
        'views/demande.xml',
        'views/service_request.xml',
        'security/ir.model.access.csv',

        
    ],
    'installable': True,
    'application': False,
    'auto_install': True,
    'license': 'LGPL-3',
}
