import json
import logging
from base64 import decodestring

from odoo import http
from odoo.http import request
from datetime import datetime
from odoo.exceptions import AccessDenied, AccessError

_logger = logging.getLogger(__name__)


class TamirciDemande(http.Controller):
    
    
    def _create_attachment(self, attachment, model_name, res_id,filename):
        attachment_value = {
            'name': filename,
            'datas': attachment,
            'res_model': model_name,
            'res_id': res_id,
            'public': True,
            'mimetype':'image/jpeg',
            'type': 'binary'
        }
            
        return http.request.env['ir.attachment'].sudo().create(attachment_value) 

    @http.route('/web/session/authenticate', type='json', auth="none")
    def get_session_info(self):
        request.session.check_security()
        request.uid = request.session.uid
        request.disable_db = False
        return request.env['ir.http'].session_info()
        
        
    @http.route('/web/session/authenticate', type='json', auth="user")
    def authenticate(self, db, login, password, base_location=None):
        request.session.authenticate(db, login, password)
        return request.env['ir.http'].session_info()
    
  
    @http.route(['/create/user/signup/'], type="json", auth="user", website=True, method=['POST'],csrf=False)
    def create_user(self, **kwargs):
        
        operate = request.env['res.users'].sudo().search([("login", "=", kwargs['telephone'])])
        
        if request.jsonrequest:
            if kwargs['partner_id'] and kwargs['telephone'] and kwargs['password'] and kwargs['re-password']:
                status = 401
                name =  kwargs['partner_id']
                login = kwargs['telephone']
                password = kwargs['password']
                repassword = kwargs['re-password']

                if login == operate:
                    msg = 'Le numéro de téléphone existe déjà'
                    msg = 'Le numéro de téléphone existe déjà'
                if password != repassword:
                    msg = 'Mot de passe non identique'
                else:
                    status = 200
                    new_user = request.env['res.users'].sudo().create({
                        "name": name,
                        "login": login,
                        "password": password,
                        'company_id': 1,
                        'in_group_38': True,
                    })
                    
                    msg = "Compte créé avec succèss"
                client = request.env['res.partner'].sudo().search([('id','=', new_user.id)])
                client.sudo().write({
                    "is_client": True,
                })
                    
            return {"status": status, "message": msg, "data": new_user}
                    
                    
    @http.route(['/update/user/image/'], auth='user', type="json", method=["POST"], csrf=False)
    def _update_image(self, **kwargs):
        if kwargs['name']:
            msg = 'Aucune image téléchargée'
            client_id = request.env['ir.http'].session_info()['uid']
            if client_id != None:
                try:
                    client = request.env['res.users'].sudo().search([('id','=', client_id)])
                    client.sudo().write({
                        "name": kwargs['name'],
                    })
                    
                except:
                    pass
        
        return {"code": 200, "message": msg}
    
    @http.route(['/get/user/info/<int:id>'], auth='user', type="json", method=["POST"], csrf=False)
    def _update_image(self, id):
        id = request.env['ir.http'].session_info()['uid']
        if id != None:
            try:
                user = request.env['res.users'].sudo().search([('id','=', id)])
                if len(user) != None:
                    error_data = 'Réussie'
                    status = 200
                    for rec in user:
                        details = {
                            "name": rec.name,
                            "login": rec.login,
                            "id": rec.id,
                        }
                else:
                    status = False
                    error_code = 1
                    error_data = 'Données non disponible!'
            except:
                pass
        return { "code": status, "message": error_data, "data": details}
        
        
    @http.route(['/get/service'], type="json", auth="none", website=True, method=['GET'],
                    csrf=False)
    def get_infos(self):
        values = {}
        data = request.env['service.name'].sudo().search([])
        if data:
            values['status'] = 200
            details = []
            for rec in data:
                get_service = {
                    "id": rec.id,
                    "name": rec.name, 
                }
                details.append(get_service)

            values['details'] = get_service
        else:
            values['success'] = False
            values['error_code'] = 1
            values['error_data'] = 'No data found!'

        return json.dumps(values)
        
        
    @http.route(['/category/<int:id>/'], type="json", auth="none", website=True, method=['GET'],
                    csrf=False)
    def _get_category(self, id=None, **kwargs):
        values = {}
        data = request.env['tamirci.category'].sudo().browse(int(id))
        if data:
            values['status'] = 200
            details = []
            for rec in data:
                category_id = {
                    "id": rec.id,
                    "name": rec.name, 
                    "description": rec.description,
                }
                details.append(category_id)

            values['details'] = category_id
        else:
            values['success'] = False
            values['error_code'] = 1
            values['error_data'] = 'No data found!'

        return json.dumps(values)
        
        
    
    
        